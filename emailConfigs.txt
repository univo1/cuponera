config mail
step 1:
go to: https://myaccount.google.com/lesssecureapps and Allow less secure apps access: YES

step 2: delete the next lines in your .env
MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

step 3: copy and paste the following lines and replace them with your personal email credentials in your .env
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=465
MAIL_USERNAME=youremailhere
MAIL_PASSWORD=emailpasswordhere
MAIL_ENCRYPTION=ssl


if you have some problem read this:
 https://stackoverflow.com/questions/57124091/error-expected-response-code-250-but-got-code-550-with-message-550-this-is-a
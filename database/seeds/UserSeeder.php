<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'	=> 'Super',
            'lastname'	=> 'Super-Administrador',
            'email' => 'admin@app.com',
            'telefono' => 77777777,
            'tipo_usuario' => 700,
            'password'=> Hash::make('Admin-123'),
            'dui' => 777777777,
            'auth' => true
        ]);
    }
}

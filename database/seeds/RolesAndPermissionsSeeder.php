<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $adminColumnName = 'admin';
    public function run()
    {
       // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

       // create permissions array
        $arrayOfPermissionNames = [
            'marca.crear',
            'marca.actualizar',
            'marca.eliminar',
            'marca.buscar',
            'carro.crear',
            'carro.actualizar',
            'carro.eliminar',
            'carro.buscar',
            'carro.editar-estado',
            'modelos.crear',
            'modelos.actualizar',
            'modelos.eliminar',
            'modelos.buscar',
            'users.crear',
            'users.actualizar',
            'users.eliminar',
            'users.buscar',
            'roles.crear',
            'roles.eliminar',
            'roles.actualizar',
            'roles.buscar',
            'permissions.crear',
            'permissions.actualizar',
            'permissions.eliminar',
            'permissions.buscar',
            'contacts.asignar',
            'contacts.completar',
            'contacts.buscar'
        ];

        // create permissions
        $permissions = $this->collectArray($arrayOfPermissionNames);
        Permission::insert($permissions->toArray());

        // create roles array by default
        $arrayOfRolesNames = [
            $this->adminColumnName,
            'vendedor'
        ];

        if($arrayOfRolesNames[0])
        {
            Role::create(['name' => $arrayOfRolesNames[0]])
                ->givePermissionTo(Permission::all());
                
            $user = User::findOrFail(1);
            $user->assignRole([$this->adminColumnName]);
            //$user->givePermissionTo(Permission::all());
        }
        Role::insert($this->collectArray($this->remove_admin($arrayOfRolesNames))->toArray());
        /*$user2 = User::findOrFail(2);
        $user2->assignRole('vendedor');*/
    }

    protected function collectArray($array)
    {
        return collect($array)->map(function ($name) {
            return ['name' => $name, 'guard_name' => 'web'];
        });
    }

    protected function remove_admin($array = [])
    {
        array_shift($array);
        return $array;
    }
}

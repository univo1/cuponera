<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rubro_id')->nullable();
            $table->integer('empresa_id')->default(0);
            $table->string('codigo');
            $table->string('nombre_empresa');
            $table->text('direccion');
            $table->text('descripcion')->nullable();
            $table->string('email')->nullable();
            $table->decimal('comision',10,2)->nullable();
            $table->foreign('rubro_id')->references('id')->on('rubros')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}

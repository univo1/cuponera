<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromocionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promociones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('empresa_id');
            $table->string('titulo');
            $table->decimal('precio_regular',10,2);
            $table->decimal('precio_oferta',10,2);
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->date('fecha_limite');
            $table->text('descripcion');
            $table->enum('estado',['espera de aprobación','aprobado','activo','pasado','rechazado','descartado']);
            $table->text('motivo_rechazo')->nullable();
            $table->integer('cantidad')->nullable();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promociones');
    }
}

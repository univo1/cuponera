<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'VentasController@index')
            ->name('ventas.index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('verify/{code}','Auth\RegisterController@verifyUser')->name('verify.user');
Route::get('create/{code}/type/{type}','ClienteController@registerCompanyUser')->name('create.user.company');
Route::get('create/branch/{code}/type/{type}','ClienteController@registerBranchUser')->name('create.user.branch');

Route::middleware(['auth'])->group(function(){
    //rutas de administracion

    //Ruta de ventas
    Route::resource('ventas','VentasController', ['except'=>'show']);
    Route::post('ventas/search','VentasController@search')
        ->name('ventas.search');
        
	//index de administracion
    Route::get('admin', 'AdminController@index')
            ->name('admin.index');

    //rutas de rubros
    Route::resource('rubros','RubrosController', ['except'=>'show']);
    Route::post('rubros/search','RubrosController@search')
        ->name('rubros.search');

    //rutas de categorias
    Route::resource('categorias','CategoriasController', ['except'=>'show']);
    Route::post('categorias/search','CategoriasController@search')
    ->name('categorias.search');

    //rutas de productos
    Route::resource('productos','ProductosController', ['except'=>'show']);
    Route::post('productos/search','ProductosController@search')
        ->name('productos.search');

    //rutas de empresas
    Route::resource('empresas','EmpresasController', ['except'=>'show']);
    Route::post('empresas/search','EmpresasController@search')
        ->name('empresas.search');
    
    //ruta de Sucursales
    Route::resource('empresa/{empresa_id}/sucursal','EmpresasController', ['except'=>'show']);
    Route::post('empresa/{empresa_id}/sucursal/search','EmpresasController@search')
        ->name('sucursal.search');

    //ruta de Roles
    Route::resource('roles','RolesController', ['except'=>'show']);
    Route::post('roles/search','RolesController@search')
        ->name('roles.search');

    //ruta de Permisos
    Route::resource('permission','PermissionController', ['except'=>'show']);
    Route::post('permission/search','PermissionController@search')
        ->name('permission.search');

    //ruta de Usuarios
    Route::resource('users','UserController', ['except'=>'show']);
    Route::post('users/search','UserController@search')
        ->name('users.search');

    //ruta de promociones
    Route::resource('empresa/{empresa_id}/promociones','PromocionesController', ['except'=>'show']);
    Route::post('empresa/{empresa_id}/promociones/search','PromocionesController@search')
        ->name('promociones.search');

});

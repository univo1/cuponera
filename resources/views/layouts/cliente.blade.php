<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ModuloV</title>
    <!-- Scripts 
    <script src="{{ asset('js/app.js') }}" defer></script> -->
    <!-- Bootstrap CSS-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet"> 
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,700&display=swap" rel="stylesheet">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/c7999f3602.js" crossorigin="anonymous"></script>
    <!-- Sweetalert2 Styles -->
    <link href="{{ asset('sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
</head>
<body>
      <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light bg-dark fixed-top" style="opacity: 0.95;">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <a class="navbar-brand" style="color: azure;" href="#">
              <i class="fas fa-home"></i>
              Cuponera
            </a>
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item">
                <a class="nav-link" style="color: azure;" href="{{ asset('/') }}">
                  <i class="fas fa-tags"></i>
                  Cupones
                </a>
              </li> 
              <li class="nav-item">
                <a class="nav-link" style="color: azure;" href="#" tabindex="-1" aria-disabled="true">
                  <i class="fas fa-user-tag"></i>
                  Mis cupones
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" style="color: azure;" href="#" tabindex="-1" aria-disabled="true">
                  <i class="fas fa-user"></i>
                  Mi cuenta
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" style="color: azure;" href="#" tabindex="-1" aria-disabled="true">
                  <i class="fas fa-shopping-cart"></i>
                  Carrito
                </a>
              </li>
              <li class="nav-item">
                <form class="form-inline position-relative d-inline-block my-2" action="{{ route($table.'.search') }}" method="POST">
                  @csrf
                  @isset($txtSearch)
                    <a href="{{ route($table.'.index') }}" class="waves-effect waves-light btn-flat left prefix">
                      <i class="fas fa-times" style="color: #e2c43f;"></i>
                    </a>
                  @endisset
                  <input class="form-control" type="text" class="validate" required name="txtSearch" id="search" value="{{ (isset($txtSearch))? $txtSearch :'' }}" placeholder="Buscar" aria-label="Buscar">
                  <button class="btn btn-warning my-2 my-sm-0" type="submit" data-toggle="tooltip" data-placement="right" title="Buscar">
                    <i class="fas fa-search"></i>
                    Buscar
                  </button>
                </form>
              </li>
              <li class="nav-item">
                <a class="nav-link" style="color: azure;" href="#" tabindex="-1" aria-disabled="true">
                  <i class="fas fa-shopping-cart"></i>
                  Cerrar sesión
                </a>
              </li>
            </ul>
          </div>
        </nav>
        <div id="contenido" class="bg-grey w-100">
          @yield('contenido')
        </div> 
      </div>
      <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
      <script src="{{ asset('js/popper.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('js/vue.js') }}"></script>
      <script src="{{ asset('js/axios.js') }}"></script>
      @yield('scripts')
</body>
</html>

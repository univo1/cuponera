@extends('layouts.form')
@section('form')
<div class="row justify-content-around">
    <div class="col-xs-12 col-md-6">
        <div class="card text-center">
            <div class="card-header">
                <div class="row">
                    <div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
                        {{$title}}
                    </div>
                    <div class="col-2">
                        <a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle=" tooltip"
                            data-placement="right" title="Cerrar formulario">
                            <i class="far fa-times-circle fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route($table.'.store',['empresa_id' => $empresa_id]) }}" method="POST" accept-charset="UTF-8"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-row justify-content-around">
                        <div class="col-md-6 mb-3">
                            <label for="titulo">Titulo</label>
                            <input type="text" class="form-control" id="titulo" value="{{ old('titulo') }}"
                                name="titulo">
                            @error('titulo')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="precio_regular">Precio regular</label>
                            <input type="text" class="form-control" id="precio_regular"
                                value="{{ old('precio_regular') }}" name="precio_regular">
                            @error('precio_regular')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="precio_oferta">Precio oferta</label>
                            <input type="text" class="form-control" id="precio_oferta"
                                value="{{ old('precio_oferta') }}" name="precio_oferta">
                            @error('precio_oferta')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="fecha_inicio">Fecha de inicio</label>
                            <input type="date" class="form-control" id="fecha_inicio" value="{{ old('fecha_inicio') }}" name="fecha_inicio">
                            @error('fecha_inicio')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="fecha_fin">Fecha fin</label>
                            <input type="date" class="form-control" id="fecha_fin" value="{{ old('fecha_fin') }}"
                                name="fecha_fin">
                            @error('fecha_fin')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="fecha_limite">Fecha Limite</label>
                            <input type="date" class="form-control" id="fecha_limite" value="{{ old('fecha_limite') }}"name="fecha_limite">
                            @error('fecha_limite')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="descripcion">Descripción</label>
                            <textarea name="descripcion" id="descripcion" class="form-control" cols="30"
                                rows="2"></textarea>
                            @error('descripcion')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <!-- Cambiar hasta aqui lo demas es igual -->
                    <div class="card-footer text-muted col-md-12">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>
@endsection

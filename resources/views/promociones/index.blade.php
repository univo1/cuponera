@extends('layouts.list')
@section('list')
<div class="card" style="width: auto;">
	<div class="card-body">
    <div class="row">
      <div class="col-auto">
        <h5 class="card-title text-uppercase font-weight-bold">
          <i class="fas fa-align-justify"></i>
          {{$title}}
        </h5>
      </div>
    </div>
	<table class="table table-borderless table-hover table-responsive-lg">
  <thead>
      <tr class="text-uppercase font-italic">
        <th scope="col">Empresa</th>
        <th scope="col">Titulo Oferta</th>
        <th scope="col">Precio regular</th>
        <th scope="col">Precio oferta</th>
        <th scope="col">Fecha inicio</th>
        <th scope="col">Fecha fin</th>
        <th scope="col">Fecha limite</th>
        <th scope="col">Descripción</th>
        <th scope="col">Estado</th>
        <th scope="col">Motivo rechazo</th>
        <th scope="col">Editar</th>
        <th scope="col">Eliminar</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $e)
      <tr>
        <td scope="row">{{ $e->nombre_empresa }}</td>
        <td scope="row">{{ $e->titulo }}</td>
        <td scope="row">${{ $e->precio_regular }}</td>
        <td scope="row">${{ $e->precio_oferta }}</td>
        <td scope="row">{{ $e->fecha_inicio }}</td>
        <td scope="row">{{ $e->fecha_fin }} </td>
        <td scope="row">{{ $e->fecha_limite }}</td>
        <td scope="row">{{ $e->descripcion }}</td>
        <td scope="row">{{ $e->estado }}</td>
        <td scope="row">{{ $e->motivo_rechazo }}</td>
        <td>
          <a href="{{ route($table.'.edit', ['empresa_id' => $empresa_id,'promocione' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
            <i class="fas fa-pen"></i>
          </a>
        </td>
        <td>
          <form action="{{ route($table.'.destroy', ['empresa_id' => $empresa_id,'promocione' => $e->id ]) }}" method="post" class="frmDelete">
            @csrf
            @method('DELETE')
            <button class="btn red-text btnDelete" type="button" tag="{{ $e->id }}" data-toggle="tooltip" data-placement="right" title="Eliminar registro">
              <i class="fas fa-eraser"></i>
            </button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
	</table>
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
	</div>
@endsection

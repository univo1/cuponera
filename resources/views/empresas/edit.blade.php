@extends('layouts.form')
@section('form')
<div class="row justify-content-around">
    <div class="col-xs-12 col-md-6">
        <div class="card text-center">
            <div class="card-header">
                <div class="row">
                    <div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
                        {{$title}}
                    </div>
                    <div class="col-2">
                        <a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle=" tooltip"
                            data-placement="right" title="Cerrar formulario">
                            <i class="far fa-times-circle fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route($table.'.update', [ 
                    $table == 'sucursal' ? 'empresa_id' : 'empresa' => $empresa_id  ?? $data->id, 
                    $table == 'sucursal' ? 'sucursal' : '' =>  $data->id ?? '']) }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-row justify-content-around">
                        <!-- Cambiar desde aqui -->
                        <div class="col-md-6 mb-3">
                            <label for="codigo">Codigo</label>
                            <input type="text" class="form-control" id="codigo" value="{{ $data->codigo }}"
                                name="codigo">
                            @error('codigo')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="nombre_empresa">Nombre de empresa</label>
                            <input type="text" class="form-control" id="nombre_empresa"
                                value="{{ $data->nombre_empresa }}" name="nombre_empresa">
                            @error('nombre_empresa')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="email">email</label>
                            <input type="text" class="form-control" id="email" value="{{ $data->email }}"
                                name="email">
                            @error('email')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        @if($table == 'empresas')
                            <div class="col-md-6 mb-3">
                                <label for="comision">comision</label>
                                <input type="text" class="form-control" id="comision" value="{{ $data->comision }}"
                                    name="comision">
                                @error('comision')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @enderror
                            </div>
                        @endif
                        @if($table == 'empresas')
                            <div class="col-md-6 mb-3">
                                <label for="rubro">Rubro de la Empresa</label>
                                <select name="rubro" id="rubro" class="form-control">
                                    <option value="">Seleccione un rubro</option>
                                    @foreach ($rubros as $rubro)
                                        <option value="{{ $rubro->id }}" {{$data->rubro_id == $rubro->id ? 'selected' : ''}}> {{ $rubro->rubro }} </option>
                                    @endforeach
                                </select>
                                @error('rubro')
                                    <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                        role="alert">
                                        <strong>{{ $message }}</strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @enderror
                            </div>
                        @endif
                        <div class="col-md-6 mb-3">
                            <label for="direccion">Dirección</label>
                            <textarea name="direccion" id="direccion" class="form-control" cols="30"
                                rows="2">{{ $data->direccion }}</textarea>
                            @error('direccion')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="descripcion">Descripción</label>
                            <textarea name="descripcion" id="descripcion" class="form-control" cols="30"
                                rows="2">{{ $data->descripcion }}</textarea>
                            @error('descripcion')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <!-- Cambiar hasta aqui lo demas es igual -->
                    </div>
                    <!-- Cambiar hasta aqui lo demas es igual -->
                    <div class="card-footer text-muted">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>
@endsection

@extends('layouts.list')
@section('list')
<div class="card" style="width: auto;">
	<div class="card-body">
    <div class="row">
      <div class="col-auto">
        <h5 class="card-title text-uppercase font-weight-bold">
          <i class="fas fa-align-justify"></i>
          {{$title}}
        </h5>
      </div>
    </div>
	<table class="table table-borderless table-hover table-responsive-lg">
  <thead>
      <tr class="text-uppercase font-italic">
        <th scope="col">{{$table}}</th>
        @if($table == 'empresas')
          <th scope="col">Rubro</th>
        @endif
        <th scope="col">Codigo</th>
        <th scope="col">Dirección</th>
        <th scope="col">Descripción</th>
        <th scope="col">email</th>
        @if($table == 'empresas')
          <th scope="col">comisión</th>
        @endif
        @if($table == 'empresas')
          <th scope="col">Sucursales</th>
        @endif
        @if($table == 'empresas')
          <th scope="col">Promociones</th>
        @endif
        <th scope="col">Editar</th>
        <th scope="col">Eliminar</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $e)
      <tr>
        <td scope="row">{{ $e->nombre_empresa }}</td>
        @if($table == 'empresas')
          <td scope="row">{{ $e->rubro }}</td>
        @endif
        <td scope="row">{{ $e->codigo }}</td>
        <td scope="row">{{ $e->direccion }}</td>
        <td scope="row">{{ $e->descripcion }}</td>
        <td scope="row">{{ $e->email }}</td>
        @if($table == 'empresas')
          <td scope="row">{{ $e->comision }} %</td>
        @endif
        @if($table == 'empresas')
          <td scope="row">
            <a href="{{ route('sucursal.index', ['empresa_id' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
              <i class="fas fa-building"></i>
            </a>
          </td>
        @endif
        @if($table == 'empresas')
          <td scope="row">
            <a href="{{ route('promociones.index', ['empresa_id' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
              <i class="fas fa-hand-holding-usd"></i>
            </a>
          </td>
        @endif
        <td>
          <a href="{{ route($table.'.edit', [$empresa_id ?? 'empresa' => $empresa_id ?? $e->id, $e->empresa_id != 0 ? 'sucursal' : '' => $e->empresa_id != 0 ? $e->id : '']) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
            <i class="fas fa-pen"></i>
          </a>
        </td>
        <td>
          <form action="{{ route($table.'.destroy', [$empresa_id ?? 'empresa' => $empresa_id ?? $e->id, $e->empresa_id != 0 ? 'sucursal' : '' => $e->empresa_id != 0 ? $e->id : '' ]) }}" method="post" class="frmDelete">
            @csrf
            @method('DELETE')
            <button class="btn red-text btnDelete" type="button" tag="{{ $e->id }}" data-toggle="tooltip" data-placement="right" title="Eliminar registro">
              <i class="fas fa-eraser"></i>
            </button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
	</table>
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
	</div>
@endsection

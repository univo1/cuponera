@extends('layouts.cliente')
@section('contenido')
<div id="content" class="container" style="padding-top: 90px;"> 
  <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
    @foreach ($data as $promocion)
    <div class="col mb-4">
      <div class="card">
        <img src="https://www.webered.com/datos/uploads/mod_publicacion/21544/coupons-5dfab563e85d7.png" style="width: 100%; height: 210px;" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title font-weight-bold text-uppercase">
            <div class="row">
              <div class="col-sm-6">
                {{ $promocion -> titulo }}
              </div>
              <div class="col-sm-6 text-right">
                <a href="carrito.html" class="btn" type="submit" title="addCar">
                  <i class="fas fa-cart-plus fa-lg"></i>
                </a>
              </div>
            </div>
          </h5>
          <div class="row card-text">
            <div class="col-sm-12 font-italic" style="color: green;">Precio: 
              <strike style="color:lightgrey">
                {{ $promocion -> precio_regular }}
              </strike>
              {{ $promocion -> precio_oferta }}
            </div>
            <div class="col-sm-12"><br>
              <h6 class="font-weight-bold">Descripción:</h6>
              <p class="text-justify">{{ $promocion -> descripcion }}</p>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <small class="text-muted">Vigencia hasta: {{ $promocion -> fecha_fin }}</small>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection
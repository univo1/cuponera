@extends('layouts.form')
@section('form')
<div class="row justify-content-around">
	<div class="col-xs-12 col-md-6">
		<div class="card text-center">
			<div class="card-header">
				<div class="row">
					<div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
						{{$title}}
					</div>
					<div class="col-2">
						<a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle="tooltip" data-placement="right" title="Cerrar formulario">
							<i class="far fa-times-circle fa-2x"></i>
						</a>
					</div>
				</div>
			</div>
			<div class="card-body">
				<form action="{{ route($table.'.update', ['producto' => $data->id]) }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
					@csrf
					@method('PUT')
					<div class="form-row justify-content-around">
						<!-- Cambiar desde aqui -->
						<div class="form-row justify-content-around">
						<div class="col-md-8 mb-3">
							<label for="producto">Nombre del Producto</label>
							<input type="text" class="form-control" id="producto" value="{{ $data->producto }}" name="producto">
							@error('producto')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;" role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
						</div>
						<div class="col-md-8 mb-3">
							<label for="categoria_id">Seleccione una Categoria</label>
							<select class="form-control" name="categoria_id" id="categoria_id">
								<option value="">Seleccione una Categoria</option>
								@foreach ($categorias as $e)
										<option value="{{ $e->id }}" {{ $e->id == $data->categoria_id ? 'selected' : ''}} >{{ $e->categoria }}</option>
								@endforeach
							</select>
							@error('categoria_id')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;" role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
						</div>
					</div>
						<!-- Cambiar hasta aqui lo demas es igual -->
					</div>
					<!-- Cambiar hasta aqui lo demas es igual -->
					<div class="card-footer text-muted">
						<button class="btn btn-primary" type="submit">Guardar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<br>
@endsection


@extends('layouts.list')
@section('list')
<div class="card" style="width: auto;">
	<div class="card-body">
    <div class="row">
      <div class="col-auto">
        <h5 class="card-title text-uppercase font-weight-bold">
          <i class="fas fa-align-justify"></i>
          {{$title}}
        </h5>
      </div>
    </div>	
	<table class="table table-borderless table-hover table-responsive-lg">
  <thead>
      <tr class="text-uppercase font-italic">
          <th scope="col">Categoria</th>
          <th scope="col">Producto</th>
          <th scope="col">Editar</th>
          <th scope="col">Eliminar</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $e)
      <tr>
        <td scope="row">{{ $e->categoria }}</td>
        <td scope="row">{{ $e->producto }}</td>
        <td>
          <a href="{{ route($table.'.edit', ['producto' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
            <i class="fas fa-pen"></i>
          </a>
        </td>
        <td>
          <form action="{{ route($table.'.destroy', ['producto' => $e->id ]) }}" method="post" class="frmDelete">
            @csrf
            @method('DELETE')
            <button class="btn red-text btnDelete" type="button" tag="{{ $e->id }}" data-toggle="tooltip" data-placement="right" title="Eliminar registro">
              <i class="fas fa-eraser"></i>
            </button>
          </form>
        </td>
        
      </tr>
      @endforeach
    </tbody>
	</table>
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
	</div>
@endsection
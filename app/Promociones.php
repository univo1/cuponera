<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promociones extends Model
{
    protected $fillable = [
        'id',
        'empresa_id',
        'titulo',
        'precio_regular',
        'precio_oferta',
        'fecha_inicio',
        'fecha_fin',
        'descripcion',
        'estado',
        'motivo_rechazo',
    ];

    protected $casts = [
        'id'               => 'Int',
        'empresa_id'       => 'Int',
        'titulo'           => 'String',
        'precio_regular'   => 'Float',
        'precio_oferta'    => 'Float',
        'fecha_inicio'     => 'Date',
        'fecha_fin'        => 'Date',
        'descripcion'      => 'String',
        'estado'           => 'Int',
        'motivo_rechazo'   => 'String',
    ];


}

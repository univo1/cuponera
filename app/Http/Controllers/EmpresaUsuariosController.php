<?php

namespace App\Http\Controllers;

use App\Empresa_usuarios;
use Illuminate\Http\Request;

class EmpresaUsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empresa_usuarios  $empresa_usuarios
     * @return \Illuminate\Http\Response
     */
    public function show(Empresa_usuarios $empresa_usuarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empresa_usuarios  $empresa_usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit(Empresa_usuarios $empresa_usuarios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empresa_usuarios  $empresa_usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empresa_usuarios $empresa_usuarios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empresa_usuarios  $empresa_usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empresa_usuarios $empresa_usuarios)
    {
        //
    }
}

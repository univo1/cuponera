<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReqProductos;
use App\Productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'productos';
    public function index()
    {
        $data = DB::table("productos")
                    ->join('categorias','productos.categoria_id','=','categorias.id')
                    ->select("productos.*",'categorias.categoria')
                    ->paginate(15);

        return view($this->table.'.index', [
            'table' =>  $this->table,
            'title' =>'Listado de Productos',
            'data'  => $data
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');
        
        $data = DB::table("productos")
                ->join('categorias','productos.categoria_id','=','categorias.id')
                ->select("productos.*",'categorias.categoria')
                ->where('producto', 'like', '%'.$r->txtSearch.'%')
                ->paginate(15);

        return view($this->table.'.index', [
            'table'     =>  $this->table,
            'title'     =>'Listado de Productos',
            'data'      => $data,
            'txtSearch' => $r->txtSearch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = DB::table("categorias")
                    ->select('categorias.*')->get();
        return view($this->table.'.create', [
            'table' =>  $this->table, 
            'title'=>'Agregar Productos',
            'categorias'=>$categorias
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReqProductos $request)
    {
        $producto        = new Productos();
        $producto->producto = $request->producto;
        $producto->categoria_id = $request->categoria_id;
        $e             = $producto->save();
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar la marca intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $productos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function edit(Productos $productos,$data)
    {   
        $categorias = DB::table("categorias")
                    ->select('categorias.*')->get();
        $productos = $productos::findOrfail($data);
        return view($this->table.'.edit', [
            'table' =>  $this->table, 
            'title'=>'Actualizar Productos',
            'categorias'=>$categorias,
            'data'=>$productos
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(ReqProductos $request, Productos $productos, $data)
    {
        $producto        = $productos::findOrfail($data);
        $producto->producto = $request->producto;
        $producto->categoria_id = $request->categoria_id;
        $e            = $producto->save();
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Se edito un registro con exito. ':'Ocurrio un problema al editar el producto intente de nuevo.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Productos $productos, $data)
    {
        $productos = $productos::findOrfail($data);
        $m = $productos->delete();
        return redirect()->route($this->table.'.index')
                        ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');

    }
}

<?php

namespace App\Http\Controllers;

use App\Ventas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = "ventas";
    public function index()
    {
        $ventas = DB::table("promociones")
                    ->select("promociones.*")
                    ->paginate(15);
                
        return view('cliente.cupones', [
            'table' => $this->table,
            'title' => 'Listado de promociones',
            'data'  =>  $ventas
        ]);
    }
    public function search(Request $r)
    {
        if (!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        $p = DB::table("promociones")
            ->join('empresa_usuarios', 'empresa_usuarios.id', '=', 'promociones.empresa_usuario_id')
            ->join('empresas', 'empresas.id', '=', 'empresa_usuarios.empresa_id')
            ->select('promociones.*', 'empresas.nombre_empresa' )
            ->where('promociones.titulo', 'like', '%'.$r->txtSearch.'%')
            ->paginate(10);

        return view('cliente.cupones', [
            'table' => $this->table,
            'title' => 'Listado de promociones',
            'data'  => $p,
            'txtSearch' => $r->txtSearch,
        ]);
    }
    public function addCar(Request $r)
    {
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ventas  $ventas
     * @return \Illuminate\Http\Response
     */
    public function show(Ventas $ventas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ventas  $ventas
     * @return \Illuminate\Http\Response
     */
    public function edit(Ventas $ventas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ventas  $ventas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ventas $ventas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ventas  $ventas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ventas $ventas)
    {
        //
    }
}

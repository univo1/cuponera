<?php

namespace App\Http\Controllers;

use App\Detalle_promociones;
use Illuminate\Http\Request;

class DetallePromocionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Detalle_promociones  $detalle_promociones
     * @return \Illuminate\Http\Response
     */
    public function show(Detalle_promociones $detalle_promociones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Detalle_promociones  $detalle_promociones
     * @return \Illuminate\Http\Response
     */
    public function edit(Detalle_promociones $detalle_promociones)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Detalle_promociones  $detalle_promociones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Detalle_promociones $detalle_promociones)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Detalle_promociones  $detalle_promociones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Detalle_promociones $detalle_promociones)
    {
        //
    }
}

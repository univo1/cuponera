<?php

namespace App\Http\Controllers;

use App\Http\Requests\reqRubros;
use App\Rubros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RubrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'rubros';
    public function index()
    {
        $rubros = DB::table("rubros")
                    ->select("rubros.*")
                    ->paginate(15);

        return view($this->table.'.index', [
            'table' =>  $this->table,
            'title' =>'Listado de Rubros',
            'data'  => $rubros
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        return view($this->table.'.index', [
            'table'     =>  $this->table,
            'title'     =>'Listado de rubros',
            'data'      => Rubros::where('rubro', 'like', '%'.$r->txtSearch.'%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table.'.create', ['table' =>  $this->table, 'title'=>'Agregar Rubros']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(reqRubros $request)
    {
        $rubros        = new Rubros();
        $rubros->rubro = $request->rubro;
        $e             = $rubros->save();
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar la marca intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rubros  $rubros
     * @return \Illuminate\Http\Response
     */
    public function show(Rubros $rubros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rubros  $rubros
     * @return \Illuminate\Http\Response
     */
    public function edit(Rubros $rubros,$data)
    {
        $rubro = $rubros::findOrfail($data);
        return view($this->table.'.edit', ['table' =>  $this->table, 'title'=>'Actualizar Rubros','data'=>$rubro]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rubros  $rubros
     * @return \Illuminate\Http\Response
     */
    public function update(reqRubros $request, Rubros $rubros, $data)
    {
        $rubro        = $rubros::findOrfail($data);
        $rubro->rubro = $request->rubro;
        $e            = $rubro->save();
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Se edito un registro con exito. ':'Ocurrio un problema al editar el rubro intente de nuevo.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rubros  $rubros
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rubros $rubros, $data)
    {
        $rurbo = $rubros::findOrfail($data);
        $m = $rurbo->delete();
        return redirect()->route($this->table.'.index')
                        ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}

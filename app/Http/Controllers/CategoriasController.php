<?php

namespace App\Http\Controllers;

use App\Categorias;
use App\Http\Requests\ReqCategorias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'categorias';
    public function index()
    {
        $categorias = DB::table("categorias")
                    ->select("categorias.*")
                    ->paginate(15);

        return view($this->table.'.index', [
            'table' =>  $this->table,
            'title' =>'Listado de Rubros',
            'data'  => $categorias
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        return view($this->table.'.index', [
            'table'     =>  $this->table,
            'title'     =>'Listado de categorias',
            'data'      => Categorias::where('categoria', 'like', '%'.$r->txtSearch.'%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table.'.create', ['table' =>  $this->table, 'title'=>'Agregar Categorias']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReqCategorias $request)
    {
        $categorias        = new Categorias();
        $categorias->categoria = $request->categoria;
        $e             = $categorias->save();
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar la marca intente de nuevo.');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function show(Categorias $categorias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function edit(Categorias $categorias, $data)
    {
        $categoria = $categorias::findOrfail($data);
        return view($this->table.'.edit', ['table' =>  $this->table, 'title'=>'Actualizar Rubros','data'=>$categoria]);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function update(ReqCategorias $request, Categorias $categorias, $data)
    {
        $categoria        = $categorias::findOrfail($data);
        $categoria->categoria = $request->categoria;
        $e            = $categoria->save();
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Se edito un registro con exito. ':'Ocurrio un problema al editar el rubro intente de nuevo.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categorias $categorias, $data)
    {
        $categoria= $categorias::findOrfail($data);
        $m = $categoria->delete();
        return redirect()->route($this->table.'.index')
                        ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}

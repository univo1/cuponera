<?php

namespace App\Http\Controllers;

use App\Empresas;
use App\Http\Requests\ReqEmpresas;
use App\Rubros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = "empresas";
    public function index($empresa_id = 0)
    {
        if($empresa_id !=0)
        {
            $p = DB::table('empresas')
                ->select("empresas.*")
                ->where('empresas.empresa_id','=' ,$empresa_id)
                ->paginate(10);

            //dd($p);

            return view($this->table . '.index', [
                'table' => 'sucursal',
                'title' => 'Listado de Sucursales',
                'data'  => $p,
                'empresa_id' => $empresa_id
            ]);
        }
        $p = DB::table('empresas')
            ->join('rubros', 'empresas.rubro_id', "=", "rubros.id")
            ->select("empresas.*", "rubros.rubro")
            ->paginate(10);

        
        return view($this->table . '.index', [
            'table' => $this->table,
            'title' => 'Listado de empresas',
            'data'  => $p,
        ]);
    }

    public function search(Request $r, $empresa_id = 0)
    {
        if (!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        if($empresa_id !=0)
        {
            $empresa = DB::table('empresas')
                ->join('rubros', 'empresas.rubro_id', "=", "rubros.id")
                ->select("empresas.*", "rubros.rubro")
                //->where('nombre_empresa', 'like', '%'.$r->txtSearch.'%')
                ->where([['empresa_id', $empresa_id],['nombre_empresa', 'like', '%'.$r->txtSearch.'%'] ])
                ->paginate(10);
        }
        $empresa = DB::table('empresas')
            ->join('rubros', 'empresas.rubro_id', "=", "rubros.id")
            ->select("empresas.*", "rubros.rubro")
            ->where('nombre_empresa', 'like', '%'.$r->txtSearch.'%')
            ->paginate(10);

        return view($this->table . '.index', [
            'table' => $this->table,
            'title' => 'Listado de empresas',
            'data'  => $empresa,
            'txtSearch' => $r->txtSearch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($empresa_id = 0)
    {
        if($empresa_id !=0)
        {
            return view($this->table.'.create', [
                'table'      => 'sucursal',
                'title'      => 'Agregar Sucursal',
                'empresa_id' => $empresa_id
            ]);
        }

        return view($this->table.'.create', [
            'table'  => $this->table,
            'title'  => 'Agregar Empresa',
            'rubros' => Rubros::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReqEmpresas $request,$empresa_id = 0)
    {
        $empresas = new Empresas();
        $empresas->rubro_id       = $request->rubro;
        $empresas->codigo         = $request->codigo;
        $empresas->nombre_empresa = $request->nombre_empresa;
        $empresas->empresa_id     = $request->empresa_id != 0 ? $empresa_id : 0;
        $empresas->direccion      = $request->direccion;
        $empresas->descripcion    = $request->descripcion;
        $empresas->email          = $request->email;
        $empresas->comision       = $request->comision;

        $e = $empresas->save(); 
        if($empresas !=null)
        {
            MailController::sendSingUpEmail($empresas->nombre_empresa, $empresas->email, $empresas->id, $empresa_id !=0 ? 644 : 640, $empresa_id !=0 ? 'create.user.branch' : 'create.user.company');
        }

        if($empresa_id !=0)
        {
            return redirect()->route('sucursal.index', ['empresa_id' => $empresa_id])->with(($e) ?
                'info' : 'danger', ($e) ?
                'Guardado con exito' : 'Ocurrio un problema al guardar la sucursal.');
        }
        
        return redirect()->route($this->table . '.index')->with(($e) ?
			'info' : 'danger', ($e) ?
			'Guardado con exito' : 'Ocurrio un problema al guardar la empresa.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function show(Empresas $empresas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function edit(Empresas $empresas, $data, $empresa_id = 0)
    {
        if($empresa_id !=0)
        {
            $empresa = $empresas::findOrFail($empresa_id);
            //dd($empresa);
            return view($this->table . '.edit', [
                'table'        => 'sucursal',
                'title'        => 'Editar Sucursal',
                'data'         => $empresa,
                'empresa_id'   => $data,
                'rubros'       => Rubros::all()
            ]);
        }
        $empresa = $empresas::findOrFail($data);
        return view($this->table . '.edit', [
			'table'        => $this->table,
			'title'        => 'Editar empresa',
			'data'         => $empresa,
			'rubros'       => Rubros::all()
		]);
	}


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function update(ReqEmpresas $request, $empresas, $empresa_id = 0)
    {
        $data                 = Empresas::findOrFail($empresa_id !=0 ? $empresa_id : $empresas);   
        $data->rubro_id       = $request->rubro;
        $data->codigo         = $request->codigo;
        $data->nombre_empresa = $request->nombre_empresa;
        $data->empresa_id     = $empresa_id != 0 ? $empresas : 0;
        $data->direccion      = $request->direccion;
        $data->descripcion    = $request->descripcion;
        $data->email          = $request->email;
        $data->comision       = $request->comision;
        $e                    = $data->save();
        if($empresa_id !=0)
        {
            return redirect()->route('sucursal.index', ['empresa_id' => $data->empresa_id])->with(($e) ?
                'info' : 'danger', ($e) ?
                'Guardado con exito' : 'Ocurrio un problema al actualizar la sucursal.');
        }
        return redirect()->route($this->table . '.index')->with(($e) ? 'info' : 'danger', ($e) ? 'Se edito un registro con exito. ' : 'Ocurrio un problema al editar la empresa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empresas $empresas, $data, $empresa_id = 0)
    {
        if($empresa_id != 0)
        {
            $empresa = $empresas::findOrfail($empresa_id);
            $m = $empresa->delete();
            return redirect()->route('sucursal.index',['empresa_id' => $data])
                        ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
        }

        $empresa = $empresas::findOrfail($data);
        $m = $empresa->delete();
        return redirect()->route($this->table.'.index')
                        ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}

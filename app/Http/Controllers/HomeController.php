<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check()){
            $user = Auth::user();
            switch ($user->tipo_usuario) {
                case 700://admin
                    return redirect()->route('admin.index');
                break;
                case 711://user
                    return redirect()->route('admin.index');
                break;
                case 640: //manager
                    return redirect()->route('admin.index');
                break;
                case 644://dependiente
                    return redirect()->route('admin.index');
                break;
                default://cliente case 666
                    return redirect()->route('admin.home');
                break;
            }
        } else return redirect()->route('cliente.home');
    }
    public function list(){
        return view('home');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\reqPromociones;
use App\Promociones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PromocionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = "promociones";
    public function index($empresa_id)
    {
        $p = DB::table("promociones")
            ->join('empresas', 'promociones.empresa_id', '=', 'empresas.id')
            ->select('promociones.*','empresas.nombre_empresa')
            ->where('promociones.empresa_id', $empresa_id)
            ->paginate(10);

            return view($this->table . '.index', [
                'table' => $this->table,
                'empresa_id' => $empresa_id,
                'title' => 'Listado de promociones',
                'data'  => $p,
            ]);

    }

    public function search(Request $r, $empresa_id)
    {
        if (!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        $p = DB::table("promociones")
            ->join('empresa_usuarios', 'empresa_usuarios.id', '=', 'promociones.empresa_usuario_id')
            ->join('empresas', 'empresas.id', '=', 'empresa_usuarios.empresa_id')
            ->select('promociones.*', 'empresas.nombre_empresa' )
            ->where('promociones.titulo', 'like', '%'.$r->txtSearch.'%')
            ->paginate(10);

        return view($this->table . '.index', [
            'table' => $this->table,
            'title' => 'Listado de empresas',
            'data'  => $p,
            'txtSearch' => $r->txtSearch,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($empresa_id)
    {
        return view($this->table.'.create', [
            'table'  => $this->table,
            'empresa_id' => $empresa_id,
            'title'  => 'Agregar promociones'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(reqPromociones $request,$empresa_id)
    {
        $promocion                  = new Promociones();
        $promocion->empresa_id      = $empresa_id;
        $promocion->titulo          = $request->titulo;
        $promocion->precio_regular  = $request->precio_regular;
        $promocion->precio_oferta   = $request->precio_oferta;
        $promocion->fecha_inicio    = $request->fecha_inicio;
        $promocion->fecha_fin       = $request->fecha_fin;
        $promocion->fecha_limite    = $request->fecha_limite;
        $promocion->descripcion     = $request->descripcion;
        $promocion->estado          = 1;

        $e = $promocion->save();

        return redirect()->route($this->table . '.index',['empresa_id' => $empresa_id])->with(($e) ?
			'info' : 'danger', ($e) ?
			'Guardado con exito' : 'Ocurrio un problema al guardar la promoción.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Promociones  $promociones
     * @return \Illuminate\Http\Response
     */
    public function show(Promociones $promociones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Promociones  $promociones
     * @return \Illuminate\Http\Response
     */
    public function edit(Promociones $promociones, $data,$empresa_id)
    {
        $promocion = DB::table('promociones')
                        ->select('promociones.id',
                                'promociones.titulo',
                                'promociones.precio_regular',
                                'promociones.precio_oferta',
                                'promociones.fecha_inicio',
                                'promociones.fecha_fin',
                                'promociones.fecha_limite',
                                'promociones.descripcion',
                                DB::raw('(promociones.estado + 0) as estado')
                                )->find($data);
        return view($this->table.'.edit', [
            'table'  => $this->table,
            'empresa_id' => $empresa_id,
            'title'  => 'Actualizar promociones',
            'data' => $promocion
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promociones  $promociones
     * @return \Illuminate\Http\Response
     */
    public function update(reqPromociones $request, $empresa_id, Promociones $promociones, $data)
    {
        $promocion = $promociones::findOrfail($data);
        $promocion->titulo          = $request->titulo;
        $promocion->precio_regular  = $request->precio_regular;
        $promocion->precio_oferta   = $request->precio_oferta;
        $promocion->fecha_inicio    = $request->fecha_inicio;
        $promocion->fecha_fin       = $request->fecha_fin;
        $promocion->fecha_limite    = $request->fecha_limite;
        $promocion->descripcion     = $request->descripcion;
        $promocion->estado          = $request->estado;
        $e = $promocion->save();

        return redirect()->route($this->table . '.index',['empresa_id' => $empresa_id])->with(($e) ?
			'info' : 'danger', ($e) ?
			'Guardado con exito' : 'Ocurrio un problema al guardar la promoción.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promociones  $promociones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promociones $promociones, $data)
    {
        $promocion = $promociones::findOrfail($data);
        $m = $promocion->delete();
        return redirect()->route($this->table.'.index')
                        ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}
